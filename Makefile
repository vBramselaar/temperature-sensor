CXX:=avr-g++
CXXFLAGS:=-Wall -std=gnu++11 -Os -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums
LDFLAGS:=
CPPFLAGS:=-I./src -I/usr/lib/avr/include/

AVRCOPY:=avr-objcopy
MCU:=attiny85
AVRFLAGS:=-mmcu=$(MCU)

AVRDUDE:=avrdude
PROGRAMMER:=arduino
PORT:=/dev/ttyACM0
BAUDRATE:=19200

SRC:=$(wildcard src/*.cpp)
OBJ:=$(patsubst %.cpp,%.o, $(SRC))

PRG:=data

.PHONY:all clean

all:$(PRG).hex

$(PRG).hex:$(PRG).elf
	$(AVRCOPY) -O ihex $^ $@

$(PRG).elf:$(OBJ)
	$(CXX) $(CXXFLAGS) $(AVRFLAGS) $(CPPFLAGS) $^ -o $@ $(LDFLAGS)

%.o:%.cpp
	$(CXX) $(CXXFLAGS) $(AVRFLAGS) $(CPPFLAGS) -c $< -o $@ $(LDFLAGS)

upload:$(PRG).hex
	$(AVRDUDE) -v -p $(MCU) -c $(PROGRAMMER) -P $(PORT) -b $(BAUDRATE) -Uflash:w:$(PRG).hex:i

serial:
	screen $(PORT) 9600

clean:
	rm -rf $(OBJ)
	rm -rf $(PRG).hex
	rm -rf $(PRG).elf
