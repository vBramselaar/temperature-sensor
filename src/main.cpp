#include <avr/io.h>
#include "gpio.h"

#define PIN 0

int main(void)
{
	Gpio::pinMode(PIN, true);
	bool led = true;
	
	while(true)
	{
		for (volatile long i = 0; i < 10000; i++);

		Gpio::digitalWrite(PIN, led);
		led = !led;
	}

	return 0;
}
