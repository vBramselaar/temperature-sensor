#ifndef BVD_GPIO_H
#define BVD_GPIO_H

#include <avr/io.h>

class Gpio
{
public:
	static void pinMode(uint8_t pin, bool mode);
	static void digitalWrite(uint8_t pin, bool mode);
	static uint8_t digitalRead(uint8_t pin);
};

#endif
