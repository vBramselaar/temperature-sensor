#include "gpio.h"

uint8_t Gpio::digitalRead(uint8_t pin)
{
	if (pin > 5)
	{
		return 0;
	}
	
	return !(PINB & (1u << pin));
}

void Gpio::digitalWrite(uint8_t pin, bool mode)
{
	if (pin > 5)
	{
		return;
	}
	
	if (mode)
	{
		PORTB |= (1u << pin);
	}
	else
	{
		PORTB &= ~(1u << pin);
	}
}

void Gpio::pinMode(uint8_t pin, bool mode)
{
	if (pin > 5)
	{
		return;
	}
	
	if (mode)
	{
		DDRB |= (1u << pin);
	}
	else
	{
		DDRB &= ~(1u << pin);
	}
} 
